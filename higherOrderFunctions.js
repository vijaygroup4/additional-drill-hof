//initialising the data
const got = {
  houses: [
    {
      name: "Starks",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Stark",
      people: [
        {
          name: "Eddard Stark",
          description:
            "Lord of Winterfell - Warden of the North - Hand of the King - Married to Catelyn (Tully) Stark",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-eddard-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Eddard_Stark",
        },
        {
          name: "Benjen Stark",
          description:
            "Brother of Eddard Stark - First ranger of the Night's Watch",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-benjen-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Benjen_Stark",
        },
        {
          name: "Robb Stark",
          description: "Son of Eddard and Catelyn Stark - Direwolf: Grey Wind",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-robb-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Robb_Stark",
        },
        {
          name: "Sansa Stark",
          description: "Daughter of Eddard and Catelyn Stark - Direwolf: Lady",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-sansa-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Sansa_Stark",
        },
        {
          name: "Arya Stark",
          description:
            "Daughter of Eddard and Catelyn Stark - Direwolf: Nymeria",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-arya-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Arya_Stark",
        },
        {
          name: "Brandon Stark",
          description: "Son of Eddard and Catelyn Stark - Direwolf: Summer",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-brandon-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Brandon_Stark",
        },
        {
          name: "Rickon Stark Upper",
          description: "Son of Eddard and Catelyn Stark - Direwolf: Shaggydog",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-rickon-stark.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Rickon_Stark",
        },
        {
          name: "Jon Snow",
          description:
            "Bastard son of Eddard Stark - Steweard in the Night's Watch - Direwolf: Ghost",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-jon-snow.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Jon_Snow",
        },
      ],
    },
    {
      name: "Lannisters",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Lannister",
      people: [
        {
          name: "Tywin Lannister",
          description: "Lord of Casterly Rock - Warden of the West",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-tywin-lannister.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Tywin_Lannister",
        },
        {
          name: "Tyrion Lannister",
          description: "Son of Tywin Lannister - The Imp",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-tyrion-lannister.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Tyrion_Lannister",
        },
        {
          name: "Jaime Lannister",
          description:
            "The Kingslayer - Knight of the Kingsgaurd - Son of Tywin Lannister",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-jaime-lannister.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Jaime_Lannister",
        },
        {
          name: "Cersei Baratheon",
          description:
            "Married to King Robert Baratheon - Daughter of Tywin Lannister",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-cersei-lannister.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Cersei_Lannister",
        },
      ],
    },
    {
      name: "Baratheons",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Baratheon",
      people: [
        {
          name: "Robert Baratheon",
          description: "The first of his name - Lord of the Seven Kingdoms",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-robert-baratheon.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Robert_Baratheon",
        },
        {
          name: "Stannis Baratheon",
          description:
            "Lord of Dragonstone - Master of Ships - Brother of Robert Baratheon",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-stannis-baratheon.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Stannis_Baratheon",
        },
        {
          name: "Renly Baratheon",
          description:
            "Lord of Storm's End - Master of Laws - Brother of Robert Baratheon",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-renly-baratheon.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Renly_Baratheon",
        },
        {
          name: "Joffrey Baratheon",
          description:
            "Heir to the Iron Throne - Son of Robert and Cersei Baratheon",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-joffrey-baratheon.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Joffrey_Baratheon",
        },
        {
          name: "Tommen Baratheon",
          description: "Son of Robert and Cersei Baratheon",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-tommen-baratheon.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Tommen_Baratheon",
        },
        {
          name: "Myrcella Baratheon",
          description: "Daughter of Robert and Cersei Baratheon",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-myrcella-baratheon.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Myrcella_Baratheon",
        },
      ],
    },
    {
      name: "Targaryens",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Targaryen",
      people: [
        {
          name: "Daenerys Targaryen",
          description:
            'Stormborn - Khaleesi of the Dothraki - The Unburnt - Mother of Dragons - Daughter of Aerys II Targaryen "The Mad King"',
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-daenerys-targaryen.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Daenerys_Targaryen",
        },
        {
          name: "Viserys Targaryen",
          description:
            'The Beggar King - Son of Aerys II Targaryen "The Mad King"',
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-viserys-targaryen.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Viserys_Targaryen",
        },
      ],
    },
    {
      name: "Greyjoys",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Greyjoy",
      people: [
        {
          name: "Balon Greyjoy",
          description: "Lord Reaper of Pyke - Head of House Greyjoy",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-balon-greyjoy.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Balon_Greyjoy",
        },
        {
          name: "Theon Greyjoy",
          description:
            "Ward of the Starks - Heir to the Iron Islands - Son of Balon Greyjoy",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-theon-greyjoy.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Theon_Greyjoy",
        },
        {
          name: "Yara Greyjoy",
          description: "Ironborn - Son of Balon Greyjoy",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-yara-greyjoy.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Yara_Greyjoy",
        },
      ],
    },
    {
      name: "Tyrells",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Tyrell",
      people: [
        {
          name: "Margaery Baratheon",
          description:
            "Wife of Renly Baratheon - Sister of Loras Tyrell - Granddaughter of Olenna Tyrell",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-margaery-tyrell.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Margaery_Tyrell",
        },
        {
          name: "Loras Tyrell",
          description:
            "Heir to Highgarden - Commander of the Kings Gaurd - Brother of Margaery Baratheon",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-loras-tyrell.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Loras_Tyrell",
        },
      ],
    },
    {
      name: "Tullys",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Tully",
      people: [
        {
          name: "Catelyn Stark",
          description: "Married to Eddard Stark - Daughter of Hoster Tully",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-catelyn-tully.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Catelyn_Tully",
        },
        {
          name: "Lysa Arryn",
          description: "Widow of Jon Arryn - Daughter of Hoster Tully",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-lysa-tully.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Lysa_Tully",
        },
        {
          name: "Edmure Tully",
          description: "Heir to Riverrun - Son of Hoster Tully",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-edmure-tully.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Edmure_Tully",
        },
        {
          name: "Brynden Tully",
          description:
            "Lord of Riverrun - Head of House Tully - Brother of Hoster Tully",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-brynden-tully.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Brynden_Tully",
        },
      ],
    },
    {
      name: "Redwyne",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Redwyne",
      people: [
        {
          name: "Olenna Tyrell",
          description: "Matriarch of House Tyrell - Queen of Thorns",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-olenna-tyrell.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Olenna_Tyrell",
        },
      ],
    },
    {
      name: "Freys",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Freys",
      people: [
        {
          name: "Walder Frey",
          description: "Lord of the Crossing - Head of House Frey",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-walder-frey.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Walder_Frey",
        },
      ],
    },
    {
      name: "Arryns",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Arryns",
      people: [
        {
          name: "Jon Arryn",
          description:
            "Lord of the Eyrie - Head of House Arryn - Warden of the East - Defender of the Vale",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-jon-arryn.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Jon_Arryn",
        },
      ],
    },
    {
      name: "Dothrakis",
      wikiLink: "http://gameofthrones.wikia.com/wiki/House_Dothrakis",
      people: [
        {
          name: "Khal Drogo",
          description:
            "Leader of the Dothraki people - Dothraki Warlord - Married to Daenerys Targaryen",
          image:
            "http://cheatsheetworld.com/img/entertainment/game-of-thrones/game-of-thrones-khal-drogo.jpg",
          wikiLink: "http://gameofthrones.wikia.com/wiki/Drogo",
        },
      ],
    },
  ],
};

//function that gives total no of people
function countAllPeople(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //finding per house how many people are there
    const people = houses.map((house) => {
      return house.people.length;
    });

    //counting total no of people
    let noOfPeople = people.reduce(
      (accumulator, currentValue, index, array) => {
        return accumulator + currentValue;
      }
    );

    return noOfPeople;
  } catch (error) {
    console.error("Error in countAllPeople:", error.message);
    return null;
  }
}

//function that returns no of people per house
function peopleByHouses(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting the people count per house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people.length;
        return accumulator;
      },
      {}
    );

    return differentHousesPeople;
  } catch (error) {
    console.error("Error in peopleByHouses:", error.message);
    return null;
  }
}

//function that returns names of all people
function everyone(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting the people names for each house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people;
        return accumulator;
      },
      {}
    );

    //pushing those each house people names into single array
    const peopleNames = Object.keys(differentHousesPeople).reduce(
      (accumulator, currentKey, index, array) => {
        differentHousesPeople[currentKey].forEach((innerObject) => {
          accumulator.push(innerObject.name);
        });
        return accumulator;
      },
      []
    );

    return peopleNames;
  } catch (error) {
    console.error("Error in everyone:", error.message);
    return null;
  }
}

//function that returns the people names having S or s
function nameWithS(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting the people names per house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people;
        return accumulator;
      },
      {}
    );

    //filtering the names of people having s or S
    const peopleNames = Object.keys(differentHousesPeople).reduce(
      (accumulator, currentKey, index, array) => {
        differentHousesPeople[currentKey].forEach((innerObject) => {
          if (
            innerObject.name.includes("s") ||
            innerObject.name.includes("S")
          ) {
            accumulator.push(innerObject.name);
          }
        });
        return accumulator;
      },
      []
    );

    return peopleNames;
  } catch (error) {
    console.error("Error in nameWithS:", error.message);
    return null;
  }
}

//function that returns the people name having A or a
function nameWithA(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting the people names per house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people;
        return accumulator;
      },
      {}
    );

    //filtering those names whose names have A
    const peopleNames = Object.keys(differentHousesPeople).reduce(
      (accumulator, currentKey, index, array) => {
        differentHousesPeople[currentKey].forEach((innerObject) => {
          if (
            innerObject.name.includes("a") ||
            innerObject.name.includes("A")
          ) {
            accumulator.push(innerObject.name);
          }
        });
        return accumulator;
      },
      []
    );

    return peopleNames;
  } catch (error) {
    console.error("Error in nameWithA:", error.message);
    return null;
  }
}

//function that returns people names whose surname starts with S
function surnameWithS(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting the people per house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people;
        return accumulator;
      },
      {}
    );

    //filtering the names who have surname starting with S
    const peopleNames = Object.keys(differentHousesPeople).reduce(
      (accumulator, currentKey, index, array) => {
        differentHousesPeople[currentKey].forEach((innerObject) => {
          const nameArray = innerObject.name.split(" ");
          let character = nameArray[1][0];
          if (character === "S") {
            accumulator.push(innerObject.name);
          }
        });
        return accumulator;
      },
      []
    );

    return peopleNames;
  } catch (error) {
    console.error("Error in surnameWithS:", error.message);
    return null;
  }
}

//function that gives names whose surname starts with A
function surnameWithA(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting the people per house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people;
        return accumulator;
      },
      {}
    );

    //filtering the names whose surname starts with A
    const peopleNames = Object.keys(differentHousesPeople).reduce(
      (accumulator, currentKey, index, array) => {
        differentHousesPeople[currentKey].forEach((innerObject) => {
          const nameArray = innerObject.name.split(" ");
          let character = nameArray[1][0];
          if (character === "A") {
            accumulator.push(innerObject.name);
          }
        });
        return accumulator;
      },
      []
    );

    return peopleNames;
  } catch (error) {
    console.error("Error in surnameWithA:", error.message);
    return null;
  }
}
//function that returns people name per house
function peopleNameOfAllHouses(object) {
  try {
    if (!object || !object.houses || !Array.isArray(object.houses)) {
      throw new Error("Invalid input object.");
    }
    const houses = object.houses;

    //getting people name per house
    const differentHousesPeople = houses.reduce(
      (accumulator, currentHouse, index, array) => {
        accumulator[currentHouse.name] = currentHouse.people.map(
          (innerObject) => {
            return innerObject.name;
          }
        );
        return accumulator;
      },
      {}
    );

    return differentHousesPeople;
  } catch (error) {
    console.error("Error in peopleNameOfAllHouses:", error.message);
    return null;
  }
}

//Executing all the functions
console.log(countAllPeople(got));
console.log(peopleByHouses(got));
console.log(everyone(got));
console.log(nameWithS(got));
console.log(nameWithA(got));
console.log(surnameWithS(got));
console.log(surnameWithA(got));
console.log(peopleNameOfAllHouses(got));
